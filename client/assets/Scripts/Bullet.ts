import { UNetMgr } from "../UNet/UNetMgr";
import FrameStep from "./FrameStep";
import Game from "./Game";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Bullet extends FrameStep {
    
    life : number = 0;
    speed : cc.Vec2 = null;

    initFrame(id:string,frame:number,data:any){
        super.initFrame(id,frame,data);
        this.node.x = data.x
        this.node.y = data.y

        this.life = 2;
        if(data.type == 0){
            this.speed = cc.v2(-500,200);
        } else if(data.type == 1){
            this.speed = cc.v2(500,200);
        }
    }

    OnSyncFrame(frame:number){
        var dframe = frame - this.lastUpdateFrame
        if(dframe <= 0){
            return
        }
        var time = (frame - this.createFrame) * UNetMgr.delay
        if(time >= this.life){
            Game.Instance.removeBullet(this)
            return;
        }
        var stepx = UNetMgr.delay * this.speed.x
        var stepy = UNetMgr.delay * this.speed.y
        var x = this.createData.x + (frame - this.createFrame)*stepx
        var y = this.createData.y + (frame - this.createFrame)*stepy

        if((this.node.x - x) / stepx > 2 || (this.node.y - y) / stepy > 2){
            this.node.x = x;
            this.node.y = y;
        }
    }

    update(dt:number){
        var x = this.node.x + this.speed.x * dt
        var y = this.node.y + this.speed.y * dt
        this.node.x = x;
        this.node.y = y;
    }
}