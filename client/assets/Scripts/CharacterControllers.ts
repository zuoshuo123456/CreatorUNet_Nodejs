// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { UNetMgr } from "../UNet/UNetMgr";

var Direction = {
    Z : cc.v2(0,0),
    RU  : cc.v2(0.7,0.7),
    R : cc.v2(1,0),
    U : cc.v2(0,1),
    LU : cc.v2(-0.7,0.7),
    L : cc.v2(-1,0),
    LD  : cc.v2(-0.7,-0.7),
    D  : cc.v2(0,-1),
    RD  : cc.v2(0.7,-0.7)
}


const {ccclass, property} = cc._decorator;

@ccclass
export default class CharacterControllers extends cc.Component {

    public lastDirection : cc.Vec2 = null;
    private lastPoint : cc.Vec2 = null;
    private _touchEnable : boolean = false
    private _keyCodes : Array<number> = []

    private isClockwise = false

    start(){
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onDeviceMotionEvent, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onDeviceMotionEvent, this);
    }
    private onDeviceMotionEvent(e){
        // cc.log('e:',e.keyCode)
        if([87,83,65,68,74,75,76].findIndex(a=>{return a == e.keyCode}) < 0){
            return
        }

        if(e.type == cc.SystemEvent.EventType.KEY_DOWN){
            // console.log('keyCode:',e.keyCode)
            if(e.keyCode == 74){
                this.node.emit('input_attack',0)
                return;
            } else if(e.keyCode == 75){ 
                this.node.emit('input_attack',1)
                return;
            }else if(e.keyCode == 76){ 
                this.node.emit('input_change',0)
                return;
            }
        }

        // cc.log('onDeviceMotionEvent:',e.keyCode)
        var idx = this._keyCodes.findIndex(item=>{return item == e.keyCode})
        if(e.type == cc.SystemEvent.EventType.KEY_DOWN){
            if(idx < 0){
                this._keyCodes.push(e.keyCode);
            } else {
                return
            }
        } else if(idx >= 0){
            this._keyCodes.splice(idx,1);
        }
        var dir = cc.v2(0,0)
        if(this._keyCodes.length){
            dir = this.getDiectionByCode(this._keyCodes[this._keyCodes.length-1])
        }
        if(this._keyCodes.length > 1){
            var d2 = this.getDiectionByCode(this._keyCodes[this._keyCodes.length-2])
            if(dir.x * d2.x < 0 || dir.y * d2.y < 0){
                dir = d2;
            } else {
                dir = cc.v2(dir.x||d2.x,dir.y||d2.y)
                if(dir.x !=0 && dir.y != 0){
                    dir.x *= 0.7
                    dir.y*= 0.7
                }
            }
        }
        if(!this.lastDirection || !this.lastDirection.equals(dir)){
            this.lastDirection = dir
            this.emitDir(dir);
        }
    }
    private emitDir(d){
        this.node.emit('control',d);
    }
    private getDiectionByCode(code){
        switch(code){
            case 87:return Direction.U;
            case 83:return Direction.D;
            case 65:return Direction.L; 
            case 68:return Direction.R;
        }
    }

    static getNearAngleDir(curAngle,nextAngle){
        let nRotation = nextAngle%360
        let nodeRotation =curAngle%360

        if(nRotation>180){
            nRotation = nRotation - 360
        }
        if(nodeRotation>180){
            nodeRotation = nodeRotation - 360
        }
        if(nRotation<-180){
            nRotation = nRotation + 360
        }
        if(nodeRotation<-180){
            nodeRotation = nodeRotation + 360
        }
        // 求得最近的运动方向
        let curDir = 0
        curDir = nRotation - nodeRotation
        if(nRotation<0 && Math.abs(nRotation+360 - nodeRotation) < Math.abs(curDir)){
            curDir = nRotation+360 - nodeRotation
        }
        else if(nodeRotation<0 && Math.abs(nRotation - (nodeRotation + 360)) < Math.abs(curDir)){
            curDir = nRotation - (nodeRotation + 360)
        }
        return curDir
    }

    update(dt){
        if(UNetMgr.model === UNetMgr.SyncModel.SelfNoDelay){
            if(this.lastDirection && !this.lastDirection.equals(cc.Vec2.ZERO)){
                let angle1 = Math.atan2(this.lastDirection.y,this.lastDirection.x) * 180 / Math.PI;
                angle1 = angle1 - 90

                let dir = CharacterControllers.getNearAngleDir(this.node.angle,angle1);
                let addValue = (dir>0?3:-3)
                if(dir == 0) addValue = 0

                var a1 = this.node.angle+addValue
                this.node.angle = a1

                if(Math.abs(dir) < 15){
                    this.node.x += this.lastDirection.x * 200 * dt;
                    this.node.y += this.lastDirection.y * 200 * dt;
                }
            }
        }
    }
}
